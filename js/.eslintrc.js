module.exports = {
    "env": {
        "node": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "rules": {
        "indent": ["error", 4],
        "linebreak-style": ["error", "unix"],
        "quotes": ["error", "double"],
        "semi": ["error", "always"],
        "init-declarations": "off",
        "no-console": "off",
        "no-inline-comments": "off",
        // "comma-dangle": ["error", "always"],
        "no-cond-assign": ["error", "always"],
    }
};
